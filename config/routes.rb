Rails.application.routes.draw do
  root 'posts#index'
  devise_for :users
  resources :users, only:[:show]
  resources :posts do
    resources :likes
    resources :comments
  end
  delete 'delete_image/:id' => 'posts#delete_image', as: 'delete_image'
end
