class PostsController < ApplicationController
    before_action :set_post, only: [:show, :edit, :update, :destroy]

    def create
        @post = Post.new(post_params)
        @post.user_id = current_user.id
        if @post.save
            redirect_to @post
        else
            render 'new'
        end
    end

    def index
        @posts = Post.all
    end

    def show
    end

    def new
        @post = Post.new
    end

    def edit
    end

    def update
        respond_to do |format|
            if @user.update(user_params)
                format.html { redirect_to @user, notice: 'User was successfully updated.' }
                format.json { render :show, status: :ok, location: @user }
            else
                format.html { render :edit }
                format.json { render json: @user.errors, status: :unprocessable_entity }
            end
        end
    end

    def destroy
        @post.destroy

        redirect_to root_path
    end

    def delete_image
        @images = ActiveStorage::Attachment.find(params[:id])
        @images.purge

        redirect_back(fallback_location: request.referer)
    end

    private

    def set_post
        @post = Post.find(params[:id])
    end

    def post_params
        params.require(:post).permit(:user_id, images: [])
    end
end
