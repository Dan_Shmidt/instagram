class CommentsController < ApplicationController
    before_action :authenticate_user!, except: [:index]
    before_action :find_post!

    def create
        @post = Post.find(params[:post_id])
        @comment = @post.comments.create(comment_params)
        redirect_back(fallback_location: root_path)
    end

    def show
        @comment = Comment.find(params[:id])
    end

    private

    def find_post!
        @post = Post.find_by_slug!(params[:post_slug])
    end

    def comment_params
        params.require(:comment).permit(:body)
    end
end
